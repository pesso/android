package br.com.voidcorp.weatherfy.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import br.com.voidcorp.weatherfy.R;
import br.com.voidcorp.weatherfy.presenter.SearchContract;
import br.com.voidcorp.weatherfy.view.holder.HolderSearchResult;

public class AdpSearchPlace extends RecyclerView.Adapter<HolderSearchResult> {

    private final SearchContract.PresenterContract mPresenter;

    public AdpSearchPlace(final SearchContract.PresenterContract presenter) {
        mPresenter = presenter;
    }

    @Override
    public HolderSearchResult onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new HolderSearchResult(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_search_result, parent, false));
    }

    @Override
    public void onBindViewHolder(final HolderSearchResult holder, final int position) {
        mPresenter.putValues(holder, position);
    }

    @Override
    public int getItemCount() {
        return mPresenter.getItemCount();
    }

}
