package br.com.voidcorp.weatherfy.view.holder;

import android.support.v7.widget.AppCompatTextView;
import android.view.View;

import br.com.voidcorp.weatherfy.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HolderFragCurrently {

    @BindView(R.id.forecast_detail_time)
    AppCompatTextView mForecastTime;

    @BindView(R.id.forecast_detail_wind)
    AppCompatTextView mForecastWind;

    public HolderFragCurrently(final View view) {
        ButterKnife.bind(this, view);
    }

    public AppCompatTextView getForecastTime() {
        return mForecastTime;
    }

    public AppCompatTextView getForecastWind() {
        return mForecastWind;
    }
}
