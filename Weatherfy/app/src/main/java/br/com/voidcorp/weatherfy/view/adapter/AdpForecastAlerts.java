package br.com.voidcorp.weatherfy.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import br.com.voidcorp.weatherfy.R;
import br.com.voidcorp.weatherfy.presenter.CurrentlyContract;
import br.com.voidcorp.weatherfy.view.holder.HolderForecastAlert;

public class AdpForecastAlerts extends RecyclerView.Adapter<HolderForecastAlert> {

    private final CurrentlyContract.PresenterContract mView;

    public AdpForecastAlerts(final CurrentlyContract.PresenterContract view) {
        mView = view;
    }

    @Override
    public HolderForecastAlert onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new HolderForecastAlert(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_forecast_alert, parent, false));
    }

    @Override
    public void onBindViewHolder(final HolderForecastAlert holder, final int position) {
        mView.putValues(holder, position);
    }

    @Override
    public int getItemCount() {
        return mView.getCount();
    }
}
