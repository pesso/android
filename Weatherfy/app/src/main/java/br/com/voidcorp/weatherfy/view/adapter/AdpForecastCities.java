package br.com.voidcorp.weatherfy.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import br.com.voidcorp.weatherfy.R;
import br.com.voidcorp.weatherfy.presenter.CitiesContract;
import br.com.voidcorp.weatherfy.presenter.ListContract;
import br.com.voidcorp.weatherfy.view.holder.HolderForecastCity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AdpForecastCities extends RecyclerView.Adapter<HolderForecastCity> {

    private final ListContract mListContract;

    private int lastPosition = -1;

    public AdpForecastCities(final ListContract presenter) {
        mListContract = presenter;
    }

    @Override
    public HolderForecastCity onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new HolderForecastCity(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_forecast_city, parent, false));
    }

    @Override
    public void onBindViewHolder(final HolderForecastCity holder, final int position) {
        mListContract.putValues(holder, position);
        if (position > lastPosition) {
            final Animation animation = AnimationUtils.loadAnimation(holder.itemView.getContext(), R.anim.forecast_item);
            holder.itemView.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return mListContract.getCount();
    }

}
