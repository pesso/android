package br.com.voidcorp.weatherfy.view.widget;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class WeatherfyTabLayout extends TabLayout {


    public WeatherfyTabLayout(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public WeatherfyTabLayout(final Context context) {
        super(context);
    }

    public void setTabSelectionEnabled(final boolean enabled) {
        for (int i = 0; i < getTabCount(); i++) {
            final ViewGroup view = (ViewGroup) getChildAt(0);
            view.setEnabled(enabled);
            view.setClickable(enabled);
            for(int j = 0; j< view.getChildCount(); j++) {
                view.getChildAt(j).setClickable(enabled);
            }
        }
    }



}
