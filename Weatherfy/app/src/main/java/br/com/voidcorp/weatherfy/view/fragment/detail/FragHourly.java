package br.com.voidcorp.weatherfy.view.fragment.detail;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.R;
import br.com.voidcorp.weatherfy.WeatherfyApplication;
import br.com.voidcorp.weatherfy.helper.net.response.ForecastResponse;
import br.com.voidcorp.weatherfy.injection.component.DaggerHourlyComponent;
import br.com.voidcorp.weatherfy.injection.module.HourlyModule;
import br.com.voidcorp.weatherfy.presenter.DetailContract;
import br.com.voidcorp.weatherfy.presenter.HourlyContract;
import br.com.voidcorp.weatherfy.view.adapter.AdpForecastCities;

public class FragHourly extends Fragment implements IFragmentDetail,HourlyContract.ViewContract {

    @Inject
    HourlyContract.PresenterContract mHourlyPresenter;

    private AdpForecastCities mAdapter;
    private DetailContract.ViewContract mParentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DaggerHourlyComponent.builder().hourlyModule(new HourlyModule(this)).build().inject(this);
        mAdapter = new AdpForecastCities(mHourlyPresenter);
        final RecyclerView hours = (RecyclerView) view;
        hours.setAdapter(mAdapter);
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        if(context instanceof DetailContract.ViewContract) {
            mParentView = (DetailContract.ViewContract) context;
        }
    }

    @Override
    public int getTitle() {
        return R.string.forecast_hourly;
    }

    @Override
    public void onForecastLoaded(final Serializable forecast, final String timezone) {
        if(forecast instanceof ForecastResponse.Hourly) {
            mHourlyPresenter.setHours((ForecastResponse.Hourly)forecast, timezone);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public WeatherfyApplication getWeatherfy() {
        return mParentView.getWeatherfy();
    }

    @Override
    public void notifyDataSetChange() {
        if(mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }
}
