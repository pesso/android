package br.com.voidcorp.weatherfy.injection.component;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.WeatherfyApplication;
import br.com.voidcorp.weatherfy.injection.module.ApiManagerModule;
import br.com.voidcorp.weatherfy.injection.module.DaoModule;
import dagger.Component;

@Singleton
@Component(modules = {ApiManagerModule.class, DaoModule.class})
public interface ApplicationComponent {

    void inject(final WeatherfyApplication application);

}
