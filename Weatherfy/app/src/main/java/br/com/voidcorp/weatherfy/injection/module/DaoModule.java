package br.com.voidcorp.weatherfy.injection.module;

import br.com.voidcorp.weatherfy.model.dao.CityDao;
import br.com.voidcorp.weatherfy.model.dao.SettingsDao;
import dagger.Module;
import dagger.Provides;
import io.realm.Realm;

@Module
public class DaoModule {

    private final Realm mRealm;

    public DaoModule(final Realm realm) {
        mRealm = realm;
    }

    @Provides
    public CityDao providesCityDao() {
        return new CityDao(mRealm);
    }

    @Provides
    public SettingsDao providesSettingsDao() {
        return new SettingsDao(mRealm);
    }

}
