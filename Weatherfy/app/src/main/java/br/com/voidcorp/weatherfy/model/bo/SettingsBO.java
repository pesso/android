package br.com.voidcorp.weatherfy.model.bo;

import io.realm.RealmObject;

public class SettingsBO extends RealmObject {

    private int mTemperatureType;

    public void setTemperatureType(final int temperatureType) {
        mTemperatureType = temperatureType;
    }

    public int getTemperatureType() {
        return mTemperatureType;
    }
}
