package br.com.voidcorp.weatherfy.helper.task;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.voidcorp.weatherfy.model.bo.CityBO;

public class TaskSearchPlace extends AsyncTask<String, Void, List<CityBO>> {

    private final OnSearchCompletedListener mListener;
    private final Context mContext;

    public TaskSearchPlace(final Context context, final OnSearchCompletedListener listener) {
        mListener = listener;
        mContext = context;
    }

    @Override
    protected List<CityBO> doInBackground(final String... params) {
        final List<CityBO> cities = new ArrayList<>(0);

        final Geocoder geocoder = new Geocoder(mContext);
        try {
            final List<Address> addresses = geocoder.getFromLocationName(params[0], 10);
            for (Address address : addresses) {
                final CityBO city = new CityBO();
                city.setName(address.getLocality());
                city.setLatitude(address.getLatitude());
                city.setLongitude(address.getLongitude());
                cities.add(city);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return cities;
    }

    @Override
    protected void onPostExecute(final List<CityBO> cities) {
        if(!isCancelled() && mListener != null) {
            mListener.onSuccess(cities);
        }
    }

    public interface OnSearchCompletedListener {

        void onSuccess(final List<CityBO> list);

    }

}
