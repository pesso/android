package br.com.voidcorp.weatherfy.view.util;


import android.support.design.widget.Snackbar;
import android.view.View;

public class SnackbarUtil {

    public static void show(final View view, final String message) {
        final Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

}
