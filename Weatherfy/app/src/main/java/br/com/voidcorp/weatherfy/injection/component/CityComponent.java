package br.com.voidcorp.weatherfy.injection.component;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.injection.module.CityModule;
import br.com.voidcorp.weatherfy.view.fragment.home.FragCities;
import dagger.Component;

@Singleton
@Component(modules = {CityModule.class})
public interface CityComponent {

    void inject(final FragCities cities);

}
