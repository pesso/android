package br.com.voidcorp.weatherfy.view.holder;


import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class HolderSearchResult extends RecyclerView.ViewHolder {
    public HolderSearchResult(final View itemView) {
        super(itemView);
    }

    public AppCompatTextView getMessage() {
        return (AppCompatTextView) itemView;
    }

}
