package br.com.voidcorp.weatherfy.injection.component;

import java.util.List;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.injection.module.HomeModule;
import br.com.voidcorp.weatherfy.presenter.HomeContract;
import br.com.voidcorp.weatherfy.view.activity.MainActivity;
import br.com.voidcorp.weatherfy.view.fragment.home.IFragmentHome;
import dagger.Component;

@Singleton
@Component(modules = {HomeModule.class})
public interface HomeComponent {

    HomeContract.PresenterContract providesPresenterContract();

    List<IFragmentHome> providesHomeContent();

    void inject(final MainActivity activity);

}
