package br.com.voidcorp.weatherfy.presenter;

import br.com.voidcorp.weatherfy.WeatherfyApplication;

public interface CitiesContract {

    interface ViewContract {

        WeatherfyApplication getWeatherfy();

        void notifyDataSetChanged();

        void notifyItemChanged(final int position);

        void notifyItemInserted(final int position);

        void notifyItemRemoved(final int position);

        void navigate(final int where);

        void setSwipeEnabled(final boolean enabled);

        void notifyDetailCity(final int id, final int position);
    }

    interface PresenterContract extends ListContract {

        void load();

        void onCitySelected(final int position);

        void notifySelected();

        void onCityDeleted(final int position);
    }

}
