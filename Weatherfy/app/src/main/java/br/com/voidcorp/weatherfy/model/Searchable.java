package br.com.voidcorp.weatherfy.model;

public interface Searchable {

    int RESULT = 0;
    int MESSAGE = 1;

    int getType();

}
