package br.com.voidcorp.weatherfy.view.fragment.detail;

import java.io.Serializable;

public interface IFragmentDetail {

    int getTitle();

    void onForecastLoaded(final Serializable forecast, final String timezone);

}
