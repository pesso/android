package br.com.voidcorp.weatherfy.injection.component;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.injection.module.SearchModule;
import br.com.voidcorp.weatherfy.view.fragment.home.FragSearch;
import dagger.Component;

@Singleton
@Component(modules = {SearchModule.class})
public interface SearchComponent {

    void inject(final FragSearch fragSearch);

}
