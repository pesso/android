package br.com.voidcorp.weatherfy.presenter;

import br.com.voidcorp.weatherfy.helper.net.response.ForecastResponse;

public interface WeeklyContract {

    int THREESHOLD_DAYS = 5;

    interface ViewContract {

        void notifyItemInserted(final int position);
    }

    interface PresenterContract extends ListContract {

        void notifyDay(final ForecastResponse.Daily daily, final String timezone);

    }

}
