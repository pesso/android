package br.com.voidcorp.weatherfy.presenter;

public interface ListContract {

    int getCount();

    void putValues(final Object holder, final int position);

}
