package br.com.voidcorp.weatherfy.presenter.impl;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.presenter.HomeContract;

public class HomePresenterImpl implements HomeContract.PresenterContract {

    private int mLastPage = -1;
    private final HomeContract.ViewContract mView;

    @Inject
    public HomePresenterImpl(final HomeContract.ViewContract view) {
        mView = view;
    }

    @Override
    public void load() {
        if (mView.getWeatherfy().getCityDao().count() == 0) {
            mView.setSwipeEnabled(false);
            mView.navigate(HomeContract.ViewContract.SEARCH);
        } else {
            mView.navigate(HomeContract.ViewContract.CITIES);
        }
    }

    @Override
    public void onPageSelected(final int position) {
        if(mLastPage > -1) {
            mView.getContent().get(mLastPage).notifyUnselected();
        }
        mView.getContent().get(position).notifySelected();
        mLastPage = position;
    }

}
