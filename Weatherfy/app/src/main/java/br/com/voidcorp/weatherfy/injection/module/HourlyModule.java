package br.com.voidcorp.weatherfy.injection.module;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.presenter.HourlyContract;
import br.com.voidcorp.weatherfy.presenter.impl.HourlyPresenterImpl;
import dagger.Module;
import dagger.Provides;

@Module
public class HourlyModule {

    private final HourlyContract.ViewContract mView;

    public HourlyModule(final HourlyContract.ViewContract view) {
        mView = view;
    }

    @Provides
    @Singleton
    HourlyContract.PresenterContract providesPresenter() {
        return new HourlyPresenterImpl(mView);
    }

}
