package br.com.voidcorp.weatherfy.presenter.impl;

import android.view.View;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.helper.net.response.ForecastResponse;
import br.com.voidcorp.weatherfy.presenter.WeeklyContract;
import br.com.voidcorp.weatherfy.view.holder.HolderForecastCity;
import br.com.voidcorp.weatherfy.view.util.ForecastIconSelector;

public class WeeklyPresenterImpl implements WeeklyContract.PresenterContract {

    private final WeeklyContract.ViewContract mView;
    private List<ForecastResponse.Daily> days;
    private String mTimezone;

    @Inject
    public WeeklyPresenterImpl(final WeeklyContract.ViewContract view) {
        mView = view;
        days = new ArrayList<>(0);
    }

    @Override
    public void notifyDay(final ForecastResponse.Daily daily, final String timezone) {
        mTimezone = timezone;
        days.add(daily);
        mView.notifyItemInserted(days.size() - 1);
    }

    @Override
    public int getCount() {
        return days.size();
    }

    @Override
    public void putValues(final Object obj, final int position) {
        if(obj instanceof HolderForecastCity) {
            final ForecastResponse.Daily daily = days.get(position);
            final ForecastResponse.Datum__ forecast = daily.data.get(0);
            final HolderForecastCity holder = (HolderForecastCity) obj;
            holder.getForecastIcon().setImageResource(ForecastIconSelector.forecast(forecast.icon));
            if(forecast.temperatureMax != null && forecast.temperatureMin != null) {
                final int temperature = (forecast.temperatureMax.intValue() + forecast.temperatureMin.intValue()) / 2;
                holder.getForecastDegree().setText(String.valueOf(temperature));
            }

            final Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(forecast.time * 1000L);

            final SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM");
            final String hours = dateFormat.format(instance.getTime());

            holder.getCityName().setText(hours);

            holder.getLoading().setVisibility(View.GONE);
            holder.getForecastError().setVisibility(View.GONE);
            holder.getForecast().setVisibility(View.VISIBLE);
            holder.getForecastDegreeType().setVisibility(View.VISIBLE);
            holder.getForecastIcon().setVisibility(View.VISIBLE);
            holder.getForecastDegree().setVisibility(View.VISIBLE);
            holder.getCityName().setVisibility(View.VISIBLE);

        }
    }
}
