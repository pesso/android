package br.com.voidcorp.weatherfy.view.fragment.home;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.R;
import br.com.voidcorp.weatherfy.WeatherfyApplication;
import br.com.voidcorp.weatherfy.injection.component.DaggerSearchComponent;
import br.com.voidcorp.weatherfy.injection.module.SearchModule;
import br.com.voidcorp.weatherfy.presenter.HomeContract;
import br.com.voidcorp.weatherfy.presenter.SearchContract;
import br.com.voidcorp.weatherfy.view.adapter.AdpSearchPlace;
import br.com.voidcorp.weatherfy.view.util.OnItemClickSupport;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FragSearch extends Fragment implements IFragmentHome, SearchContract.ViewContract {

    private final SearchView.OnQueryTextListener mOnQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(final String query) {
            mSearchPresenter.search(query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(final String newText) {
            return false;
        }
    };

    private final OnItemClickSupport.OnItemClickListener mOnItemClickListener = new OnItemClickSupport.OnItemClickListener() {
        @Override
        public void onItemClicked(final RecyclerView recyclerView, final int position, final View v) {
            mSearchPresenter.onPlaceSelected(position);
        }
    };

    private SearchView mSearchView;
    private MenuItem mSearchMenu;

    @BindView(R.id.search_results_list)
    RecyclerView mSearchResults;

    @Inject
    SearchContract.PresenterContract mSearchPresenter;
    private AdpSearchPlace mAdapter;

    private HomeContract.ViewContract mHomeView;

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DaggerSearchComponent.builder().searchModule(new SearchModule(this)).build().inject(this);
        ButterKnife.bind(this, view);

        mAdapter = new AdpSearchPlace(mSearchPresenter);
        mSearchResults.setAdapter(mAdapter);

        OnItemClickSupport.addTo(mSearchResults).setOnItemClickListener(mOnItemClickListener);

    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        if (context instanceof HomeContract.ViewContract) {
            mHomeView = (HomeContract.ViewContract) context;
        }
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
        inflater.inflate(R.menu.search, menu);
        mSearchMenu = menu.findItem(R.id.search_city);
        mSearchView = (SearchView) mSearchMenu.getActionView();
        mSearchView.setOnQueryTextListener(mOnQueryTextListener);
        mSearchView.setQueryHint(getString(R.string.search_city));
        mSearchView.onActionViewExpanded();
    }

    @OnClick(R.id.find_on_map)
    void onFindMap() {

    }

    @Override
    public void notifySelected() {
        if (mSearchView != null) {
            mSearchView.onActionViewExpanded();
        }
        notifyDataSetChanged();
    }

    @Override
    public void notifyUnselected() {
        if (mSearchView != null) {
            mSearchView.onActionViewCollapsed();
        }
        mSearchPresenter.clear();
    }

    @Override
    public int getTitle() {
        return R.string.navigation_add_city;
    }

    @Override
    public int getIcon() {
        return R.drawable.search;
    }

    @Override
    public WeatherfyApplication getWeatherfy() {
        return mHomeView.getWeatherfy();
    }

    @Override
    public void showError(final String message) {
        mHomeView.showError(message);
    }

    @Override
    public void notifyDataSetChanged() {
        if(mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void navigate(final int where) {
        mHomeView.navigate(where);
    }

    @Override
    public void setSwipeEnabled(final boolean enabled) {
        mHomeView.setSwipeEnabled(enabled);
    }
}
