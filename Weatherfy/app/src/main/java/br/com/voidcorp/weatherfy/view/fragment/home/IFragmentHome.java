package br.com.voidcorp.weatherfy.view.fragment.home;

public interface IFragmentHome {

    void notifySelected();

    void notifyUnselected();

    int getTitle();

    int getIcon();

}
