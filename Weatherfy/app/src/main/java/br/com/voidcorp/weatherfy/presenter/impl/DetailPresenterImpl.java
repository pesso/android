package br.com.voidcorp.weatherfy.presenter.impl;

import android.view.MenuItem;

import java.util.Calendar;
import java.util.TimeZone;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.R;
import br.com.voidcorp.weatherfy.helper.net.response.ForecastResponse;
import br.com.voidcorp.weatherfy.model.bo.CityBO;
import br.com.voidcorp.weatherfy.model.bo.CurrentlyBO;
import br.com.voidcorp.weatherfy.presenter.DetailContract;
import br.com.voidcorp.weatherfy.presenter.WeeklyContract;
import br.com.voidcorp.weatherfy.view.fragment.detail.FragCurrently;
import br.com.voidcorp.weatherfy.view.util.ForecastIconSelector;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPresenterImpl implements DetailContract.PresenterContract {

    private final DetailContract.ViewContract mView;
    private CityBO mCity;

    @Inject
    public DetailPresenterImpl(final DetailContract.ViewContract view) {
        mView = view;
    }

    @Override
    public void load(final int cityID) {
        mCity = mView.getWeatherfy().getCityDao().getCity(cityID);
        mView.getSupportActionBar().setTitle(mCity.getName());
        final Calendar instance = Calendar.getInstance(TimeZone.getTimeZone(mCity.getTimezone()));

        if (mCity.getCurrentlyWeather() != null) {
            loadWeather(mCity.getCurrentlyWeather());
            final long time = instance.getTimeInMillis() / 1000L;
            loadWeather(time);
        }

        loadDays(instance, mCity.getTimezone());
    }

    private void loadWeather(final long time) {
        mView.getWeatherfy().getApi().getForecastAsync(mCity.getPosition(), time, new Callback<ForecastResponse>() {
            @Override
            public void onResponse(final Call<ForecastResponse> call, final Response<ForecastResponse> response) {
                if (response.isSuccessful()) {
                    final CurrentlyBO currently = response.body().currently;
                    loadWeather(currently);
                    mView.getWeatherfy().getCityDao().save(mCity, currently, response.body().timezone);
                    load(response.body());
                }
            }

            @Override
            public void onFailure(final Call<ForecastResponse> call, final Throwable t) {
                mView.showError(mView.getString(R.string.error_city_unavailable));
            }
        });
    }

    private void loadDays(final Calendar instance, final String timezone) {
        for (int i = 0; i < WeeklyContract.THREESHOLD_DAYS; i++) {
            instance.add(Calendar.DATE, 1);
            final long day = instance.getTimeInMillis() / 1000L;
            mView.getWeatherfy().getApi().getForecastDayAsync(mCity.getPosition(), day, new Callback<ForecastResponse>() {
                @Override
                public void onResponse(final Call<ForecastResponse> call, final Response<ForecastResponse> response) {
                    if(response.isSuccessful()) {
                        mView.getContent().get(DetailContract.ViewContract.WEEKLY).onForecastLoaded(response.body().daily, timezone);
                    }
                }

                @Override
                public void onFailure(final Call<ForecastResponse> call, final Throwable t) {

                }
            });

        }
    }

    private void reload() {
        final Calendar calendar = Calendar.getInstance();
        final long time = calendar.getTimeInMillis() / 1000L;
        loadWeather(time);
        loadDays(calendar, mCity.getTimezone());
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        if (item.getItemId() == R.id.forecast_delete) {
            mView.notifyDelete();
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            mView.notifyFinish();
            return true;
        } else if (item.getItemId() == R.id.forecast_refresh) {
            reload();
            return true;
        }
        return false;
    }



    private void loadWeather(final CurrentlyBO weather) {
        mView.getHolder().getForecastIcon().setImageResource(ForecastIconSelector.forecast(weather.icon));
        final Double temperature = weather.temperature;
        if (temperature != null) {
            mView.getHolder().getForecastDetailDegree().setText(String.valueOf(temperature.intValue()));
        }
        mView.getHolder().getForecastDetailDegreeType().setImageResource(ForecastIconSelector.degree(mCity.getDegreeType()));
    }

    private void load(final ForecastResponse response) {
        final FragCurrently detail = (FragCurrently) mView.getContent().get(DetailContract.ViewContract.CURRENTLY);
        detail.onForecastLoaded(response.currently, response.timezone);
        detail.setAlerts(response.alerts);
        mView.getContent().get(DetailContract.ViewContract.HOURLY).onForecastLoaded(response.hourly, response.timezone);
        mView.getContent().get(DetailContract.ViewContract.WEEKLY).onForecastLoaded(response.daily, response.timezone);
    }
}
