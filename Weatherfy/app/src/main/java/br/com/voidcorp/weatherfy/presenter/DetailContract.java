package br.com.voidcorp.weatherfy.presenter;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import java.util.List;

import br.com.voidcorp.weatherfy.WeatherfyApplication;
import br.com.voidcorp.weatherfy.view.fragment.detail.IFragmentDetail;
import br.com.voidcorp.weatherfy.view.holder.HolderDetailActivity;

public interface DetailContract {

    interface ViewContract {

        int CURRENTLY = 0;
        int HOURLY = 1;
        int WEEKLY = 2;

        FragmentManager getSupportFragmentManager();

        List<IFragmentDetail> getContent();

        HolderDetailActivity getHolder();

        WeatherfyApplication getWeatherfy();

        String getString(final int resID);

        ActionBar getSupportActionBar();

        void notifyDelete();

        void showError(final String message);

        void notifyFinish();
    }

    interface PresenterContract {

        void load(final int cityID);

        boolean onOptionsItemSelected(final MenuItem item);

    }

}
