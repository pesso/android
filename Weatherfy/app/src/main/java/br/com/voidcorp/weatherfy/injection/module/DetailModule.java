package br.com.voidcorp.weatherfy.injection.module;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.presenter.DetailContract;
import br.com.voidcorp.weatherfy.presenter.impl.DetailPresenterImpl;
import br.com.voidcorp.weatherfy.view.adapter.AdpDetailPages;
import br.com.voidcorp.weatherfy.view.fragment.detail.FragCurrently;
import br.com.voidcorp.weatherfy.view.fragment.detail.FragHourly;
import br.com.voidcorp.weatherfy.view.fragment.detail.FragWeekly;
import br.com.voidcorp.weatherfy.view.fragment.detail.IFragmentDetail;
import dagger.Module;
import dagger.Provides;

@Module
public class DetailModule {

    private final DetailContract.ViewContract mView;

    public DetailModule(final DetailContract.ViewContract view) {
        mView = view;
    }

    @Provides
    @Singleton
    DetailContract.PresenterContract providesPresenterContract() {
        return new DetailPresenterImpl(mView);
    }

    @Provides
    AdpDetailPages providesAdapter() {
        return new AdpDetailPages(mView);
    }

    @Provides
    List<IFragmentDetail> providesHomeContent() {
        final List<IFragmentDetail> fragments = new ArrayList<>(3);
        fragments.add(new FragCurrently());
        fragments.add(new FragHourly());
        fragments.add(new FragWeekly());
        return fragments;
    }

}
