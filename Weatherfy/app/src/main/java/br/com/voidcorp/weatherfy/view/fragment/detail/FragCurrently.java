package br.com.voidcorp.weatherfy.view.fragment.detail;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.R;
import br.com.voidcorp.weatherfy.helper.net.response.ForecastResponse;
import br.com.voidcorp.weatherfy.injection.component.DaggerCurrentlyComponent;
import br.com.voidcorp.weatherfy.injection.module.CurrentlyModule;
import br.com.voidcorp.weatherfy.model.bo.CurrentlyBO;
import br.com.voidcorp.weatherfy.presenter.CurrentlyContract;
import br.com.voidcorp.weatherfy.view.adapter.AdpForecastAlerts;
import br.com.voidcorp.weatherfy.view.holder.HolderFragCurrently;

public class FragCurrently extends Fragment implements IFragmentDetail, CurrentlyContract.ViewContract {

    @Inject
    CurrentlyContract.PresenterContract mCurrentlyPresenter;

    AdpForecastAlerts mAdapter;

    private HolderFragCurrently mHolder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_currently, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DaggerCurrentlyComponent.builder().currentlyModule(new CurrentlyModule(this)).build().inject(this);
        mAdapter = new AdpForecastAlerts(mCurrentlyPresenter);
        mHolder = new HolderFragCurrently(view);
    }

    @Override
    public int getTitle() {
        return R.string.forecast_currrently;
    }

    @Override
    public void onForecastLoaded(final Serializable forecast, final String timezone) {
        if (forecast instanceof CurrentlyBO) {
            mCurrentlyPresenter.notifyCurrently((CurrentlyBO) forecast);
        }
    }

    public void setAlerts(final List<ForecastResponse.Alert> alets) {
        mCurrentlyPresenter.setAlerts(alets);
    }

    @Override
    public void notifyAlerts() {
        if(mAdapter != null) {
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public HolderFragCurrently getHolder() {
        return mHolder;
    }
}
