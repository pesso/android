package br.com.voidcorp.weatherfy;

import android.app.Application;

import java.util.List;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.injection.component.DaggerApplicationComponent;
import br.com.voidcorp.weatherfy.injection.module.ApiManagerModule;
import br.com.voidcorp.weatherfy.injection.module.DaoModule;
import br.com.voidcorp.weatherfy.model.bo.CityBO;
import br.com.voidcorp.weatherfy.model.dao.CityDao;
import br.com.voidcorp.weatherfy.model.dao.SettingsDao;
import br.com.voidcorp.weatherfy.helper.net.ApiManager;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class WeatherfyApplication extends Application {

    @Inject
    ApiManager mApiManager;

    @Inject
    CityDao mCityDao;

    @Inject
    SettingsDao mSettingsDao;

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        final RealmConfiguration.Builder builder = new RealmConfiguration.Builder();
        builder.name("weatherfy.realm");
        builder.initialData(new Realm.Transaction() {
            @Override
            public void execute(final Realm realm) {
                final List<CityBO> list = CityDao.getInitialData();
                realm.copyToRealmOrUpdate(list);
            }
        });
        final RealmConfiguration configuration = builder.build();
        Realm.setDefaultConfiguration(configuration);

        DaggerApplicationComponent.builder()
                .apiManagerModule(new ApiManagerModule())
                .daoModule(new DaoModule(Realm.getDefaultInstance()))
                .build()
                .inject(this);

    }

    public ApiManager getApi() {
        return mApiManager;
    }

    public CityDao getCityDao() {
        return mCityDao;
    }

    public SettingsDao getSettingsDao() {
        return mSettingsDao;
    }
}
