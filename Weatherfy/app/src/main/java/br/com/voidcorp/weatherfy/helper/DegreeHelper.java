package br.com.voidcorp.weatherfy.helper;


public class DegreeHelper {

    public static double toCelsius(final double degree) {
        return (degree - 32) * (5 / 9);
    }

    public static double toFahrenheit(final double degree) {
        return degree * 9/5 + 32;
    }

}
