package br.com.voidcorp.weatherfy.model.bo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import io.realm.RealmObject;

public class CurrentlyBO extends RealmObject implements Serializable {

    @SerializedName("time")
    @Expose
    public Integer time;
    @SerializedName("summary")
    @Expose
    public String summary;
    @SerializedName("icon")
    @Expose
    public String icon;
    @SerializedName("nearestStormDistance")
    @Expose
    public Double nearestStormDistance;
    @SerializedName("nearestStormBearing")
    @Expose
    public Double nearestStormBearing;
    @SerializedName("precipIntensity")
    @Expose
    public Double precipIntensity;
    @SerializedName("precipProbability")
    @Expose
    public Double precipProbability;
    @SerializedName("temperature")
    @Expose
    public Double temperature;
    @SerializedName("apparentTemperature")
    @Expose
    public Double apparentTemperature;
    @SerializedName("dewPoint")
    @Expose
    public Double dewPoint;
    @SerializedName("humidity")
    @Expose
    public Double humidity;
    @SerializedName("windSpeed")
    @Expose
    public Double windSpeed;
    @SerializedName("windBearing")
    @Expose
    public Double windBearing;
    @SerializedName("visibility")
    @Expose
    public Double visibility;
    @SerializedName("cloudCover")
    @Expose
    public Double cloudCover;
    @SerializedName("pressure")
    @Expose
    public Double pressure;
    @SerializedName("ozone")
    @Expose
    public Double ozone;

}
