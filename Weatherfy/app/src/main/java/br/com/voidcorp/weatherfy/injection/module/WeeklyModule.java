package br.com.voidcorp.weatherfy.injection.module;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.presenter.WeeklyContract;
import br.com.voidcorp.weatherfy.presenter.impl.WeeklyPresenterImpl;
import dagger.Module;
import dagger.Provides;

@Module
public class WeeklyModule {

    private final WeeklyContract.ViewContract mView;

    public WeeklyModule(final WeeklyContract.ViewContract view) {
        mView = view;
    }

    @Provides
    @Singleton
    WeeklyContract.PresenterContract providesPresenter() {
        return new WeeklyPresenterImpl(mView);
    }

}
