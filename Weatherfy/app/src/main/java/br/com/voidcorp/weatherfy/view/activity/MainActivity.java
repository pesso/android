package br.com.voidcorp.weatherfy.view.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.List;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.R;
import br.com.voidcorp.weatherfy.WeatherfyApplication;
import br.com.voidcorp.weatherfy.injection.component.DaggerHomeComponent;
import br.com.voidcorp.weatherfy.injection.module.HomeModule;
import br.com.voidcorp.weatherfy.presenter.HomeContract;
import br.com.voidcorp.weatherfy.view.adapter.AdpMainPages;
import br.com.voidcorp.weatherfy.view.fragment.home.IFragmentHome;
import br.com.voidcorp.weatherfy.view.util.SnackbarUtil;
import br.com.voidcorp.weatherfy.view.widget.WeatherfyTabLayout;
import br.com.voidcorp.weatherfy.view.widget.WeatherfyViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements HomeContract.ViewContract {

    private final ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(final int position) {
            mHomePresenter.onPageSelected(position);
        }

        @Override
        public void onPageScrollStateChanged(final int state) {

        }
    };


    @BindView(R.id.tabs)
    WeatherfyTabLayout mTabs;

    @BindView(R.id.main_view_pager)
    WeatherfyViewPager mViewPager;

    @Inject
    HomeContract.PresenterContract mHomePresenter;

    @Inject
    List<IFragmentHome> mHomeContent;

    @Inject
    AdpMainPages mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        DaggerHomeComponent.builder().homeModule(new HomeModule(this)).build().inject(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setup();

        mHomePresenter.load();

    }

    private void setup() {
        mViewPager.setAdapter(mAdapter);
        mTabs.setupWithViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(mOnPageChangeListener);
    }

    @Override
    public void navigate(final int where) {
        mViewPager.setCurrentItem(where);
    }

    @Override
    public void setSwipeEnabled(final boolean enabled) {
        mTabs.setTabSelectionEnabled(enabled);
        mViewPager.setSwipeEnabled(enabled);
    }

    @Override
    public void showError(final String message) {
        SnackbarUtil.show(mViewPager, message);
    }

    @Override
    public WeatherfyApplication getWeatherfy() {
        return (WeatherfyApplication) getApplication();
    }

    @Override
    public List<IFragmentHome> getContent() {
        return mHomeContent;
    }

    @Override
    public ViewPager getViewPager() {
        return mViewPager;
    }


}
