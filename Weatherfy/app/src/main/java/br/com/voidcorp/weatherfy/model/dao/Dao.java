package br.com.voidcorp.weatherfy.model.dao;

import io.realm.Realm;

abstract class Dao<T> {

    private final Realm mRealm;

    public Dao(final Realm realm) {
        mRealm = realm;
    }

    protected final Realm getRealm() {
        return mRealm;
    }

    public abstract T createBO();

    public abstract void save(final T bo);

}
