package br.com.voidcorp.weatherfy.injection.module;

import android.view.View;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.view.holder.HolderDetailActivity;
import dagger.Module;
import dagger.Provides;

@Module
public class HolderDetailModule {

    private final View mView;

    public HolderDetailModule(final View root) {
        mView = root;
    }

    @Provides
    @Singleton
    HolderDetailActivity providesHolder() {
        return new HolderDetailActivity(mView);
    }

}
