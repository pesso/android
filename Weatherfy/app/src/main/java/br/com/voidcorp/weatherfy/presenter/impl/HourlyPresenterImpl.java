package br.com.voidcorp.weatherfy.presenter.impl;

import android.view.View;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.helper.net.response.ForecastResponse;
import br.com.voidcorp.weatherfy.presenter.HourlyContract;
import br.com.voidcorp.weatherfy.view.holder.HolderForecastCity;
import br.com.voidcorp.weatherfy.view.util.ForecastIconSelector;

public class HourlyPresenterImpl implements HourlyContract.PresenterContract {

    private final HourlyContract.ViewContract mView;
    private ForecastResponse.Hourly mForecast;
    private String mTimezone;

    @Inject
    public HourlyPresenterImpl(final HourlyContract.ViewContract view) {
        mView = view;
    }

    @Override
    public void load() {

    }

    @Override
    public void setHours(final ForecastResponse.Hourly forecast, final String timezone) {
        mForecast = forecast;
        mTimezone = timezone;
        mView.notifyDataSetChange();
    }

    @Override
    public int getCount() {
        return mForecast == null || mForecast.data == null ? 0 : mForecast.data.size();
    }

    @Override
    public void putValues(final Object obj, final int position) {
        if(obj instanceof HolderForecastCity) {
            final ForecastResponse.Datum_ hour = mForecast.data.get(position);
            final HolderForecastCity holder = (HolderForecastCity) obj;
            holder.getForecastIcon().setImageResource(ForecastIconSelector.forecast(hour.icon));
            final Double temperature = hour.temperature;
            if(temperature != null) {
                holder.getForecastDegree().setText(String.valueOf(temperature.intValue()));
            }

            final Calendar instance = Calendar.getInstance();
            instance.setTimeInMillis(hour.time * 1000L);

            final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            final String hours = dateFormat.format(instance.getTime());

            holder.getCityName().setText(hours);

            holder.getLoading().setVisibility(View.GONE);
            holder.getForecastError().setVisibility(View.GONE);
            holder.getForecast().setVisibility(View.VISIBLE);
            holder.getForecastDegreeType().setVisibility(View.VISIBLE);
            holder.getForecastIcon().setVisibility(View.VISIBLE);
            holder.getForecastDegree().setVisibility(View.VISIBLE);
            holder.getCityName().setVisibility(View.VISIBLE);

        }
    }
}
