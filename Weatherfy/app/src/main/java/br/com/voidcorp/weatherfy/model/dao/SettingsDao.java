package br.com.voidcorp.weatherfy.model.dao;

import br.com.voidcorp.weatherfy.model.bo.SettingsBO;
import io.realm.Realm;

public class SettingsDao extends Dao<SettingsBO> {

    public SettingsDao(final Realm realm) {
        super(realm);
    }

    @Override
    public SettingsBO createBO() {
        return getRealm().createObject(SettingsBO.class);
    }

    @Override
    public void save(final SettingsBO bo) {

    }

}
