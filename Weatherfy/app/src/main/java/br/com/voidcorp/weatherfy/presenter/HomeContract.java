package br.com.voidcorp.weatherfy.presenter;

import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;

import java.util.List;

import br.com.voidcorp.weatherfy.WeatherfyApplication;
import br.com.voidcorp.weatherfy.view.fragment.home.IFragmentHome;

public interface HomeContract {

    interface ViewContract {

        int CITIES = 0;
        int SEARCH = 1;

        void navigate(final int where);

        void setSwipeEnabled(final boolean enabled);

        void showError(final String message);

        WeatherfyApplication getWeatherfy();

        List<IFragmentHome> getContent();

        ViewPager getViewPager();

        FragmentManager getSupportFragmentManager();

    }

    interface PresenterContract {

        void load();

        void onPageSelected(final int position);

    }

}
