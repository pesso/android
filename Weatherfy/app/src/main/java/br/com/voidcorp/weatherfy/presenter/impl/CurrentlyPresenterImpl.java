package br.com.voidcorp.weatherfy.presenter.impl;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.R;
import br.com.voidcorp.weatherfy.helper.net.response.ForecastResponse;
import br.com.voidcorp.weatherfy.model.bo.CurrentlyBO;
import br.com.voidcorp.weatherfy.presenter.CurrentlyContract;
import br.com.voidcorp.weatherfy.view.holder.HolderForecastAlert;

public class CurrentlyPresenterImpl implements CurrentlyContract.PresenterContract {

    private final CurrentlyContract.ViewContract mView;
    private final ArrayList<ForecastResponse.Alert> mAlerts;

    @Inject
    public CurrentlyPresenterImpl(final CurrentlyContract.ViewContract view) {
        mView = view;
        mAlerts = new ArrayList<>(1);

        final ForecastResponse.Alert alert = new ForecastResponse.Alert();
        alert.description = "This place has none alerts.";

        mAlerts.add(alert);
    }

    @Override
    public void notifyCurrently(final CurrentlyBO forecast) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(forecast.time * 1000L);
        final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd HH:mm");
        mView.getHolder().getForecastTime().setText(dateFormat.format(calendar.getTime()));
        final String wind = mView.getContext().getString(R.string.wind_formatter, String.valueOf(forecast.windSpeed.doubleValue()));
        mView.getHolder().getForecastWind().setText(wind);
    }

    @Override
    public void setAlerts(final List<ForecastResponse.Alert> alerts) {
        if(alerts != null && !alerts.isEmpty()) {
            mAlerts.clear();
            mAlerts.addAll(alerts);
            mView.notifyAlerts();
        }
    }


    @Override
    public int getCount() {
        return mAlerts.size();
    }

    @Override
    public void putValues(final Object obj, final int position) {
        if(obj instanceof HolderForecastAlert) {
            final HolderForecastAlert holder = (HolderForecastAlert) obj;
            final ForecastResponse.Alert alert = mAlerts.get(position);
            holder.getMessage().setText(alert.description);
        }
    }
}
