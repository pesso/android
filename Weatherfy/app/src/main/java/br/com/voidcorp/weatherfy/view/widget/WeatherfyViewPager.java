package br.com.voidcorp.weatherfy.view.widget;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class WeatherfyViewPager extends ViewPager {

    private boolean mIsSwipeEnabled;

    public WeatherfyViewPager(final Context context) {
        super(context);
        mIsSwipeEnabled = true;
    }

    public WeatherfyViewPager(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        mIsSwipeEnabled = true;
    }

    public void setSwipeEnabled(final boolean enabled) {
        mIsSwipeEnabled = enabled;
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        return mIsSwipeEnabled && super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(final MotionEvent event) {
        return mIsSwipeEnabled && super.onInterceptTouchEvent(event);
    }

}
