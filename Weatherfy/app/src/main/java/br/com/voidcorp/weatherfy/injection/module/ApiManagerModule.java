package br.com.voidcorp.weatherfy.injection.module;

import br.com.voidcorp.weatherfy.helper.net.ApiManager;
import dagger.Module;
import dagger.Provides;

@Module
public class ApiManagerModule {

    @Provides
    public ApiManager providesApiManager() {
        return new ApiManager();
    }

}
