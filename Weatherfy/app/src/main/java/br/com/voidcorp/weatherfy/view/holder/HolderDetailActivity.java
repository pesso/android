package br.com.voidcorp.weatherfy.view.holder;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.voidcorp.weatherfy.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HolderDetailActivity {

    @BindView(R.id.forecast_detail_toolbar)
    Toolbar mToolbar;

    @BindView(R.id.forecast_detail_tabs)
    TabLayout mForecastTypeTab;

    @BindView(R.id.forecast_detail_view_pager)
    ViewPager mViewPager;

    @BindView(R.id.forecast_detail_degree)
    TextView mForecastDetailDegree;

    @BindView(R.id.forecast_detail_degree_type)
    ImageView mForecastDetailDegreeType;

    @BindView(R.id.forecast_detail_icon)
    ImageView mForecastIcon;

    public HolderDetailActivity(final View view) {
        ButterKnife.bind(this, view);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    public ViewPager getViewPager() {
        return mViewPager;
    }

    public ImageView getForecastIcon() {
        return mForecastIcon;
    }

    public TextView getForecastDetailDegree() {
        return mForecastDetailDegree;
    }

    public ImageView getForecastDetailDegreeType() {
        return mForecastDetailDegreeType;
    }

    public TabLayout getForecastTypeTab() {
        return mForecastTypeTab;
    }
}
