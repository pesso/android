package br.com.voidcorp.weatherfy.model.bo;

import br.com.voidcorp.weatherfy.model.Searchable;

public class MessageBO implements Searchable {

    private final String mMessage;
    private final boolean mError;

    public MessageBO(final String message, final boolean error) {
        mMessage = message;
        mError = error;
    }

    public String getMessage() {
        return mMessage;
    }

    public boolean isError() {
        return mError;
    }

    @Override
    public int getType() {
        return MESSAGE;
    }
}
