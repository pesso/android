package br.com.voidcorp.weatherfy.injection.component;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.injection.module.WeeklyModule;
import br.com.voidcorp.weatherfy.view.fragment.detail.FragWeekly;
import dagger.Component;

@Singleton
@Component(modules = {WeeklyModule.class})
public interface WeeklyComponent {

    void inject(final FragWeekly weekly);

}
