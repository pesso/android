package br.com.voidcorp.weatherfy.view.util;

import br.com.voidcorp.weatherfy.R;

public class ForecastIconSelector {

    public static int forecast(final String iconName) {
        switch (iconName) {
            case "clear-day":
                return R.drawable.sun;
            case "clear-night":
                return R.drawable.moon;
            case "rain":
                return R.drawable.cloud_rain;
            case "snow":
                return R.drawable.snowflake;
            case "sleet":
                return R.drawable.cloud_snow_alt;
            case "wind":
                return R.drawable.wind;
            case "fog":
                return R.drawable.cloud_fog;
            case "cloudy":
                return R.drawable.cloud;
            case "partly-cloudy-day":
                return R.drawable.cloud_sun;
            case "partly-cloudy-night":
                return R.drawable.cloud_moon;
            case "hail":
                return R.drawable.cloud_hail;
            case "thunderstorm":
                return R.drawable.cloud_lightning;
            case "tornado":
                return R.drawable.tornado;
            default:
                return 0;
        }
    }

    public static int moon(final double lunation) {
        if(lunation == 0) {
            return R.drawable.moon_new;
        } else if(lunation == 0.25) {
            return R.drawable.moon_first_quarter;
        } else if(lunation == 0.5) {
            return R.drawable.moon_full;
        } else if(lunation == 0.75) {
            return R.drawable.moon_last_quarter;
        } else if(lunation > 0 && lunation < 0.25) {
            return R.drawable.moon_waxing_crescent;
        } else if(lunation > 0.25 && lunation < 0.5) {
            return R.drawable.moon_waxing_gibbous;
        } else if(lunation > 0.5 && lunation < 0.75) {
            return R.drawable.moon_waning_gibbous;
        } else if(lunation > 0.75) {
            return R.drawable.moon_waning_crescent;
        } else {
            return 0;
        }
    }

    public static int precip(final String iconName) {
        switch (iconName) {
            case "rain":
                return R.drawable.cloud_rain;

            case "snow":
                return R.drawable.cloud_snow;

            case "sleet":
                return R.drawable.cloud_snow_alt;

            default:
                return 0;
        }
    }

    public static int degree(final int type) {
        if(type == 0) {
            return R.drawable.degrees_fahrenheit;
        } else if(type == 1) {
            return R.drawable.degrees_celcius;
        } else {
            return 0;
        }
    }

}
