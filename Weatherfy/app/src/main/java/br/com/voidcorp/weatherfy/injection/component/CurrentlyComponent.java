package br.com.voidcorp.weatherfy.injection.component;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.injection.module.CurrentlyModule;
import br.com.voidcorp.weatherfy.view.fragment.detail.FragCurrently;
import dagger.Component;

@Singleton
@Component(modules = {CurrentlyModule.class})
public interface CurrentlyComponent {

    void inject(final FragCurrently currently);

}
