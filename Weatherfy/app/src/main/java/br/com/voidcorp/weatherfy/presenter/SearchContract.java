package br.com.voidcorp.weatherfy.presenter;

import android.content.Context;

import br.com.voidcorp.weatherfy.WeatherfyApplication;
import br.com.voidcorp.weatherfy.view.holder.HolderSearchResult;

public interface SearchContract {

    interface ViewContract {

        WeatherfyApplication getWeatherfy();

        void showError(final String message);

        Context getContext();

        void notifyDataSetChanged();

        void navigate(final int where);

        void setSwipeEnabled(final boolean enabled);
    }

    interface PresenterContract {

        void search(final String place);

        void onPlaceSelected(final int position);

        void clear();

        int getItemCount();

        void putValues(final HolderSearchResult holder, final int position);

        int getItemViewType(final int position);
    }

}
