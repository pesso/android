package br.com.voidcorp.weatherfy.view.fragment.detail;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.R;
import br.com.voidcorp.weatherfy.helper.net.response.ForecastResponse;
import br.com.voidcorp.weatherfy.injection.component.DaggerWeeklyComponent;
import br.com.voidcorp.weatherfy.injection.module.WeeklyModule;
import br.com.voidcorp.weatherfy.presenter.WeeklyContract;
import br.com.voidcorp.weatherfy.view.adapter.AdpForecastCities;

public class FragWeekly extends Fragment implements IFragmentDetail, WeeklyContract.ViewContract {

    @Inject
    WeeklyContract.PresenterContract mWeeklyPresenter;

    private AdpForecastCities mAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.list, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        DaggerWeeklyComponent.builder().weeklyModule(new WeeklyModule(this)).build().inject(this);
        final RecyclerView list = (RecyclerView) view;
        mAdapter = new AdpForecastCities(mWeeklyPresenter);
        list.setAdapter(mAdapter);
    }

    @Override
    public int getTitle() {
        return R.string.forecast_weekly;
    }

    @Override
    public void onForecastLoaded(final Serializable forecast, final String timezone) {
        if (forecast instanceof ForecastResponse.Daily) {
            mWeeklyPresenter.notifyDay((ForecastResponse.Daily) forecast, timezone);
        }
    }

    @Override
    public void notifyItemInserted(final int position) {
        if (mAdapter != null) {
            mAdapter.notifyItemInserted(position);
        }
    }
}
