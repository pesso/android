package br.com.voidcorp.weatherfy.injection.module;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.presenter.HomeContract;
import br.com.voidcorp.weatherfy.presenter.impl.HomePresenterImpl;
import br.com.voidcorp.weatherfy.view.adapter.AdpMainPages;
import br.com.voidcorp.weatherfy.view.fragment.home.FragCities;
import br.com.voidcorp.weatherfy.view.fragment.home.FragSearch;
import br.com.voidcorp.weatherfy.view.fragment.home.IFragmentHome;
import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {

    private final HomeContract.ViewContract mView;

    public HomeModule(final HomeContract.ViewContract view) {
        mView = view;
    }

    @Provides
    @Singleton
    HomeContract.PresenterContract providesPresenterContract() {
        return new HomePresenterImpl(mView);
    }

    @Provides
    List<IFragmentHome> providesHomeContent() {
        final List<IFragmentHome> fragments = new ArrayList<>(2);
        fragments.add(new FragCities());
        fragments.add(new FragSearch());
        return fragments;
    }

    @Provides
    AdpMainPages providesAdpMainPages() {
        return new AdpMainPages(mView);
    }

}
