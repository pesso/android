package br.com.voidcorp.weatherfy.injection.component;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.injection.module.DetailModule;
import br.com.voidcorp.weatherfy.injection.module.HolderDetailModule;
import br.com.voidcorp.weatherfy.view.activity.CityDetailActivity;
import dagger.Component;

@Singleton
@Component(modules = {DetailModule.class, HolderDetailModule.class})
public interface DetailComponent {

//    DetailContract.PresenterContract providesPresenterContract();
//
//    List<IFragmentDetail> providesHomeContent();

    void inject(final CityDetailActivity activity);

}
