package br.com.voidcorp.weatherfy.view.fragment.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.R;
import br.com.voidcorp.weatherfy.WeatherfyApplication;
import br.com.voidcorp.weatherfy.injection.component.DaggerCityComponent;
import br.com.voidcorp.weatherfy.injection.module.CityModule;
import br.com.voidcorp.weatherfy.presenter.CitiesContract;
import br.com.voidcorp.weatherfy.presenter.HomeContract;
import br.com.voidcorp.weatherfy.view.activity.CityDetailActivity;
import br.com.voidcorp.weatherfy.view.adapter.AdpForecastCities;
import br.com.voidcorp.weatherfy.view.util.OnItemClickSupport;

public class FragCities extends Fragment implements IFragmentHome, CitiesContract.ViewContract {

    private final OnItemClickSupport.OnItemClickListener mOnItemClickListener = new OnItemClickSupport.OnItemClickListener() {
        @Override
        public void onItemClicked(final RecyclerView recyclerView, final int position, final View v) {
            mCitiesPresenter.onCitySelected(position);
        }
    };

    private RecyclerView mCitiesList;

    @Inject
    CitiesContract.PresenterContract mCitiesPresenter;

    private AdpForecastCities mAdpForecastCities;

    private HomeContract.ViewContract mHomeView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        DaggerCityComponent.builder().cityModule(new CityModule(this)).build().inject(this);
        return inflater.inflate(R.layout.list, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        mCitiesList = (RecyclerView) view;
        mAdpForecastCities = new AdpForecastCities(mCitiesPresenter);
        mCitiesList.setAdapter(mAdpForecastCities);
        OnItemClickSupport.addTo(mCitiesList).setOnItemClickListener(mOnItemClickListener);
        mCitiesPresenter.load();
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        if (context instanceof HomeContract.ViewContract) {
            mHomeView = (HomeContract.ViewContract) context;
        }
    }

    @Override
    public void notifySelected() {
        mCitiesPresenter.notifySelected();
    }

    @Override
    public void notifyUnselected() {
    }

    @Override
    public int getTitle() {
        return R.string.navigation_city;
    }

    @Override
    public int getIcon() {
        return R.drawable.city;
    }

    @Override
    public WeatherfyApplication getWeatherfy() {
        return mHomeView.getWeatherfy();
    }

    @Override
    public void notifyDataSetChanged() {
        mAdpForecastCities.notifyDataSetChanged();
    }

    @Override
    public void notifyItemChanged(final int position) {
        mAdpForecastCities.notifyItemChanged(position);
    }

    @Override
    public void notifyItemInserted(int position) {
        if (mAdpForecastCities != null) {
            mAdpForecastCities.notifyItemInserted(position);
        }
    }

    @Override
    public void notifyItemRemoved(final int position) {
        if (mAdpForecastCities != null) {
            mAdpForecastCities.notifyItemRemoved(position);
        }
    }

    @Override
    public void navigate(final int where) {
        mHomeView.navigate(where);
    }

    @Override
    public void setSwipeEnabled(final boolean enabled) {
        mHomeView.setSwipeEnabled(enabled);
    }

    @Override
    public void notifyDetailCity(final int id, final int position) {
        final Intent intent = new Intent(getContext(), CityDetailActivity.class);
        intent.putExtra(CityDetailActivity.EXTRA_CITY_ID, id);
        intent.putExtra(CityDetailActivity.EXTRA_CITY_POSITION, position);
        startActivityForResult(intent, CityDetailActivity.REQUEST_CODE);
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CityDetailActivity.REQUEST_CODE && resultCode == CityDetailActivity.RESULT_OK) {
            mCitiesPresenter.onCityDeleted(data.getIntExtra(CityDetailActivity.EXTRA_CITY_POSITION, -1));
        }
    }
}
