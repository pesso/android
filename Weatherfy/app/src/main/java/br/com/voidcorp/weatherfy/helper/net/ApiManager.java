package br.com.voidcorp.weatherfy.helper.net;

import android.support.annotation.IntDef;
import android.support.annotation.StringDef;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.GsonBuilder;

import br.com.voidcorp.weatherfy.BuildConfig;
import br.com.voidcorp.weatherfy.helper.net.response.ForecastResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiManager {

    private static final String FILTER_FORMATTER = "%s,%s";
    private static final String PARAMS_FORMATTER = "%s,%s,%s";

    private final ForecastAPI mForecastAPI;

    public ApiManager() {
        final GsonBuilder gBuilder = new GsonBuilder();
        gBuilder.setLenient();

        final Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl(BuildConfig.ENPOINT);
        builder.addConverterFactory(GsonConverterFactory.create(gBuilder.create()));
        final Retrofit retrofit = builder.build();
        mForecastAPI = retrofit.create(ForecastAPI.class);
    }

    public void getForecastAsync(final LatLng position, final long time, final Callback<ForecastResponse> callback) {
        final String params = String.format(PARAMS_FORMATTER, position.latitude, position.latitude, time);
        final Call<ForecastResponse> forecast = mForecastAPI.getForecast(BuildConfig.SECRET_KEY, params);
        forecast.enqueue(callback);
    }

    public void getForecastDayAsync(final LatLng position, final long day, final Callback<ForecastResponse> callback) {
        final String params = String.format(PARAMS_FORMATTER, position.latitude, position.latitude, day);
        final Call<ForecastResponse> call = mForecastAPI.getDailyForecast(BuildConfig.SECRET_KEY, params);
        call.enqueue(callback);
    }

    public void getForecastFilteredAsync(final LatLng position, final Callback<ForecastResponse> callback, @Filter final String... filters) {
        final String params = String.format(FILTER_FORMATTER, position.latitude, position.latitude);
        final Call<ForecastResponse> forecast = mForecastAPI.getForecastFiltered(BuildConfig.SECRET_KEY, params, getFilter(filters));
        forecast.enqueue(callback);
    }

    private String getFilter(final String... filters) {
        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < filters.length; i++) {
            final String filter = filters[i];
            builder.append(filter);
            if (i < filters.length - 1) {
                builder.append(",");
            }
        }
        return builder.toString();
    }

    @StringDef({Filter.CURRENTLY, Filter.DAILY, Filter.MINUTELY, Filter.FLAGS, Filter.HOURLY})
    public @interface Filter {

        String CURRENTLY = "currently";
        String MINUTELY = "minutely";
        String DAILY = "daily";
        String HOURLY = "hourly";
        String FLAGS = "flags";

    }

    @IntDef({Degree.CELSIUS, Degree.FAHRENHEIT})
    public @interface Degree {

        int CELSIUS = 0;
        int FAHRENHEIT = 1;

    }

}
