package br.com.voidcorp.weatherfy.injection.module;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.presenter.CitiesContract;
import br.com.voidcorp.weatherfy.presenter.impl.CitiesPresenterImpl;
import dagger.Module;
import dagger.Provides;

@Module
public class CityModule {

    private final CitiesContract.ViewContract mView;

    public CityModule(final CitiesContract.ViewContract view) {
        mView = view;
    }

    @Provides
    @Singleton
    CitiesContract.PresenterContract providesPresenterContract() {
        return new CitiesPresenterImpl(mView);
    }

}
