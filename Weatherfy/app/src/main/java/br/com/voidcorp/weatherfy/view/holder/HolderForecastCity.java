package br.com.voidcorp.weatherfy.view.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import br.com.voidcorp.weatherfy.R;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HolderForecastCity extends RecyclerView.ViewHolder {

    @BindView(R.id.forecast)
    View mForecast;

    @BindView(R.id.forecast_city_name)
    TextView mCityName;

    @BindView(R.id.forecast_icon)
    ImageView mForecastIcon;

    @BindView(R.id.forecast_degree)
    TextView mForecastDegree;

    @BindView(R.id.forecast_degree_type)
    ImageView mForecastDegreeType;

    @BindView(R.id.forecast_loading)
    ProgressBar mLoading;

    @BindView(R.id.forecast_error)
    TextView mForecastError;

    public HolderForecastCity(final View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public ProgressBar getLoading() {
        return mLoading;
    }

    public ImageView getForecastIcon() {
        return mForecastIcon;
    }

    public TextView getCityName() {
        return mCityName;
    }

    public ImageView getForecastDegreeType() {
        return mForecastDegreeType;
    }

    public TextView getForecastDegree() {
        return mForecastDegree;
    }

    public TextView getForecastError() {
        return mForecastError;
    }

    public View getForecast() {
        return mForecast;
    }
}
