package br.com.voidcorp.weatherfy.presenter;

import br.com.voidcorp.weatherfy.WeatherfyApplication;
import br.com.voidcorp.weatherfy.helper.net.response.ForecastResponse;

public interface HourlyContract {

    interface ViewContract {

        WeatherfyApplication getWeatherfy();

        void notifyDataSetChange();
    }

    interface PresenterContract extends ListContract {

        void load();

        void setHours(final ForecastResponse.Hourly forecast, final String timezone);
    }

}
