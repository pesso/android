package br.com.voidcorp.weatherfy.helper.net.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import br.com.voidcorp.weatherfy.model.bo.CurrentlyBO;
import io.realm.RealmObject;

public class ForecastResponse {

    @SerializedName("latitude")
    @Expose
    public Double latitude;
    @SerializedName("longitude")
    @Expose
    public Double longitude;
    @SerializedName("timezone")
    @Expose
    public String timezone;
    @SerializedName("offset")
    @Expose
    public Integer offset;
    @SerializedName("currently")
    @Expose
    public CurrentlyBO currently;
    @SerializedName("minutely")
    @Expose
    public Minutely minutely;
    @SerializedName("hourly")
    @Expose
    public Hourly hourly;
    @SerializedName("daily")
    @Expose
    public Daily daily;
    @SerializedName("alerts")
    @Expose
    public List<Alert> alerts = null;
    @SerializedName("flags")
    @Expose
    public Flags flags;

    public static class Flags implements Serializable {

        @SerializedName("sources")
        @Expose
        public List<String> sources = null;
        @SerializedName("darksky-stations")
        @Expose
        public List<String> darkskyStations = null;
        @SerializedName("lamp-stations")
        @Expose
        public List<String> lampStations = null;
        @SerializedName("isd-stations")
        @Expose
        public List<String> isdStations = null;
        @SerializedName("madis-stations")
        @Expose
        public List<String> madisStations = null;
        @SerializedName("units")
        @Expose
        public String units;

    }

    public static class Alert implements Serializable {

        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("time")
        @Expose
        public Integer time;
        @SerializedName("expires")
        @Expose
        public Integer expires;
        @SerializedName("description")
        @Expose
        public String description;
        @SerializedName("uri")
        @Expose
        public String uri;

    }

    public static class Daily implements Serializable {

        @SerializedName("summary")
        @Expose
        public String summary;
        @SerializedName("icon")
        @Expose
        public String icon;
        @SerializedName("data")
        @Expose
        public List<Datum__> data = null;

    }

    public static class Datum implements Serializable {

        @SerializedName("time")
        @Expose
        public Integer time;
        @SerializedName("precipIntensity")
        @Expose
        public Double precipIntensity;
        @SerializedName("precipProbability")
        @Expose
        public Double precipProbability;
        @SerializedName("precipIntensityError")
        @Expose
        public Double precipIntensityError;
        @SerializedName("precipType")
        @Expose
        public String precipType;

    }

    public static class Datum_ implements Serializable {

        @SerializedName("time")
        @Expose
        public Integer time;
        @SerializedName("summary")
        @Expose
        public String summary;
        @SerializedName("icon")
        @Expose
        public String icon;
        @SerializedName("precipIntensity")
        @Expose
        public Double precipIntensity;
        @SerializedName("precipProbability")
        @Expose
        public Double precipProbability;
        @SerializedName("precipType")
        @Expose
        public String precipType;
        @SerializedName("temperature")
        @Expose
        public Double temperature;
        @SerializedName("apparentTemperature")
        @Expose
        public Double apparentTemperature;
        @SerializedName("dewPoint")
        @Expose
        public Double dewPoint;
        @SerializedName("humidity")
        @Expose
        public Double humidity;
        @SerializedName("windSpeed")
        @Expose
        public Double windSpeed;
        @SerializedName("windBearing")
        @Expose
        public Double windBearing;
        @SerializedName("visibility")
        @Expose
        public Double visibility;
        @SerializedName("cloudCover")
        @Expose
        public Double cloudCover;
        @SerializedName("pressure")
        @Expose
        public Double pressure;
        @SerializedName("ozone")
        @Expose
        public Double ozone;

    }

    public static class Datum__ implements Serializable {

        @SerializedName("time")
        @Expose
        public Integer time;
        @SerializedName("summary")
        @Expose
        public String summary;
        @SerializedName("icon")
        @Expose
        public String icon;
        @SerializedName("sunriseTime")
        @Expose
        public Integer sunriseTime;
        @SerializedName("sunsetTime")
        @Expose
        public Integer sunsetTime;
        @SerializedName("moonPhase")
        @Expose
        public Double moonPhase;
        @SerializedName("precipIntensity")
        @Expose
        public Double precipIntensity;
        @SerializedName("precipIntensityMax")
        @Expose
        public Double precipIntensityMax;
        @SerializedName("precipIntensityMaxTime")
        @Expose
        public Integer precipIntensityMaxTime;
        @SerializedName("precipProbability")
        @Expose
        public Double precipProbability;
        @SerializedName("precipType")
        @Expose
        public String precipType;
        @SerializedName("temperatureMin")
        @Expose
        public Double temperatureMin;
        @SerializedName("temperatureMinTime")
        @Expose
        public Integer temperatureMinTime;
        @SerializedName("temperatureMax")
        @Expose
        public Double temperatureMax;
        @SerializedName("temperatureMaxTime")
        @Expose
        public Integer temperatureMaxTime;
        @SerializedName("apparentTemperatureMin")
        @Expose
        public Double apparentTemperatureMin;
        @SerializedName("apparentTemperatureMinTime")
        @Expose
        public Integer apparentTemperatureMinTime;
        @SerializedName("apparentTemperatureMax")
        @Expose
        public Double apparentTemperatureMax;
        @SerializedName("apparentTemperatureMaxTime")
        @Expose
        public Integer apparentTemperatureMaxTime;
        @SerializedName("dewPoint")
        @Expose
        public Double dewPoint;
        @SerializedName("humidity")
        @Expose
        public Double humidity;
        @SerializedName("windSpeed")
        @Expose
        public Double windSpeed;
        @SerializedName("windBearing")
        @Expose
        public Double windBearing;
        @SerializedName("visibility")
        @Expose
        public Double visibility;
        @SerializedName("cloudCover")
        @Expose
        public Double cloudCover;
        @SerializedName("pressure")
        @Expose
        public Double pressure;
        @SerializedName("ozone")
        @Expose
        public Double ozone;

    }

    public static class Hourly implements Serializable {

        @SerializedName("summary")
        @Expose
        public String summary;
        @SerializedName("icon")
        @Expose
        public String icon;
        @SerializedName("data")
        @Expose
        public List<Datum_> data = null;

    }

    public static class Minutely implements Serializable {

        @SerializedName("summary")
        @Expose
        public String summary;
        @SerializedName("icon")
        @Expose
        public String icon;
        @SerializedName("data")
        @Expose
        public List<Datum> data = null;

    }

}
