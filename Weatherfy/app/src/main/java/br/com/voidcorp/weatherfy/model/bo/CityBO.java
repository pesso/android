package br.com.voidcorp.weatherfy.model.bo;

import com.google.android.gms.maps.model.LatLng;

import br.com.voidcorp.weatherfy.model.Searchable;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class CityBO extends RealmObject implements Searchable {

    @PrimaryKey
    private int mID;

    private double mLatitude;

    private double mLongitude;

    private String mName;

    private String mTimezone;

    private CurrentlyBO mCurrentlyWeather;

    private int mDegreeType;

    @Ignore
    private boolean mIsLoading;

    @Ignore
    private boolean mError;

    public void setID(final int id) {
        mID = id;
    }

    public int getID() {
        return mID;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        mLatitude = latitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        mLongitude = longitude;
    }

    public LatLng getPosition() {
        return new LatLng(getLatitude(), getLongitude());
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public CurrentlyBO getCurrentlyWeather() {
        return mCurrentlyWeather;
    }

    public void setCurrentlyWeather(final CurrentlyBO currentlyWeather) {
        mCurrentlyWeather = currentlyWeather;
    }

    public boolean isLoading() {
        return mIsLoading;
    }

    public void setLoading(final boolean isLoading) {
        mIsLoading = isLoading;
    }

    public boolean isError() {
        return mError;
    }

    public void setError(final boolean error) {
        mError = error;
    }

    public int getDegreeType() {
        return mDegreeType;
    }

    public void setmDegreeType(final int degreeType) {
        mDegreeType = degreeType;
    }

    @Override
    public int getType() {
        return RESULT;
    }

    public String getTimezone() {
        return mTimezone;
    }

    public void setTimezone(final String timezone) {
        mTimezone = timezone;
    }
}
