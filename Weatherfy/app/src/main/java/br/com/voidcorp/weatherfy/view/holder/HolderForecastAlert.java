package br.com.voidcorp.weatherfy.view.holder;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class HolderForecastAlert extends RecyclerView.ViewHolder {
    public HolderForecastAlert(final View itemView) {
        super(itemView);
    }

    public AppCompatTextView getMessage() {
        return (AppCompatTextView) itemView;
    }

}
