package br.com.voidcorp.weatherfy.injection.component;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.injection.module.HourlyModule;
import br.com.voidcorp.weatherfy.view.fragment.detail.FragHourly;
import dagger.Component;

@Singleton
@Component(modules = {HourlyModule.class})
public interface HourlyComponent {

    void inject(final FragHourly hourly);

}
