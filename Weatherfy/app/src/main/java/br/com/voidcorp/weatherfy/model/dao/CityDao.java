package br.com.voidcorp.weatherfy.model.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.voidcorp.weatherfy.model.bo.CityBO;
import br.com.voidcorp.weatherfy.model.bo.CurrentlyBO;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class CityDao extends Dao<CityBO> {

    public CityDao(final Realm realm) {
        super(realm);
    }

    @Override
    public CityBO createBO() {
        return getRealm().createObject(CityBO.class, newID());
    }

    public long count() {
        return getRealm().where(CityBO.class).count();
    }

    public int newID() {
        final Number id = getRealm().where(CityBO.class).max("mID");
        if (id == null) {
            return 0;
        } else {
            return id.intValue() + 1;
        }
    }

    public static List<CityBO> getInitialData() {
        final List<CityBO> cities = new ArrayList<>(0);
        final CityBO dublin = new CityBO();
        dublin.setID(0);
        dublin.setName("Dublin");
        dublin.setLatitude(53.3242381);
        dublin.setLongitude(-6.3857872);

        final CityBO london = new CityBO();
        london.setID(1);
        london.setName("London");
        london.setLatitude(51.528308);
        london.setLongitude(-0.3817805);

        final CityBO newYork = new CityBO();
        newYork.setID(2);
        newYork.setName("New York");
        newYork.setLatitude(40.705311);
        newYork.setLongitude(-74.2581928);

        final CityBO barcelona = new CityBO();
        barcelona.setID(3);
        barcelona.setName("Barcelona");
        barcelona.setLatitude(41.3947688);
        barcelona.setLongitude(2.0787276);

        cities.add(dublin);
        cities.add(london);
        cities.add(newYork);
        cities.add(barcelona);

        return cities;
    }

    public List<CityBO> getCities() {
        return getRealm().where(CityBO.class).findAll();
    }

    public List<CityBO> getCities(final RealmChangeListener<RealmResults<CityBO>> listener) {
        final RealmResults<CityBO> all = getRealm().where(CityBO.class).findAll();
        all.addChangeListener(listener);
        return all;
    }

    @Override
    public void save(final CityBO city) {
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(final Realm realm) {
                realm.copyToRealmOrUpdate(city);
            }
        });
    }

    public void save(final CityBO city, final CurrentlyBO currently, final String timezone) {
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(final Realm realm) {
                final CurrentlyBO bo = realm.copyToRealm(currently);
                city.setCurrentlyWeather(bo);
                city.setTimezone(timezone);
                realm.copyToRealmOrUpdate(city);
            }
        });
    }

    public void delete(final CityBO city) {
        getRealm().beginTransaction();
        if (city.getCurrentlyWeather() != null) {
            city.getCurrentlyWeather().deleteFromRealm();
        }
        city.deleteFromRealm();
        getRealm().commitTransaction();
    }

    public boolean isCityAvailable(final String cityName) {
        return getRealm().where(CityBO.class).equalTo("mName", cityName).count() == 0;
    }

    public CityBO getCity(final int id) {
        return getRealm().where(CityBO.class).equalTo("mID", id).findFirst();
    }
}
