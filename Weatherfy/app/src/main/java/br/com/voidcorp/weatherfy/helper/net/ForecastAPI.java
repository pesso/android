package br.com.voidcorp.weatherfy.helper.net;

import br.com.voidcorp.weatherfy.helper.net.response.ForecastResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

interface ForecastAPI {

    /***
     * Retrives the forecast for a place.
     *
     * @param key    The api key
     * @param params The latitude, longitude and time (in millis)
     */
    @GET("/forecast/{key}/{params}")
    Call<ForecastResponse> getForecast(@Path("key") final String key, @Path("params") final String params);

    /***
     * Retrives the forecast for specifc day of a place.
     *
     * @param key    The api key
     * @param params The latitude, longitude and time (in millis)
     */
    @GET("/forecast/{key}/{params}?exclude=currently,hourly,alerts,flags,minutely")
    Call<ForecastResponse> getDailyForecast(@Path("key") final String key, @Path("params") final String params);

    /***
     * Retrives the forecast for a place.
     *
     * @param key    The api key
     * @param params The latitude, longitude and time (in millis)
     */
    @GET("/forecast/{key}/{params}")
    Call<ForecastResponse> getForecastFiltered(@Path("key") final String key, @Path("params") final String params, @Query("exclude") final String exclusions);

}
