package br.com.voidcorp.weatherfy.injection.module;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.presenter.SearchContract;
import br.com.voidcorp.weatherfy.presenter.impl.SearchPresenterImpl;
import dagger.Module;
import dagger.Provides;

@Module
public class SearchModule {

    private final SearchContract.ViewContract mView;

    public SearchModule(final SearchContract.ViewContract view) {
        mView = view;
    }

    @Provides
    @Singleton
    SearchContract.PresenterContract providesPresenterContract() {
        return new SearchPresenterImpl(mView);
    }

}
