package br.com.voidcorp.weatherfy.view.adapter;

import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;

import br.com.voidcorp.weatherfy.presenter.HomeContract;
import br.com.voidcorp.weatherfy.view.fragment.home.IFragmentHome;

public class AdpMainPages extends FragmentStatePagerAdapter {

    private final HomeContract.ViewContract mView;

    public AdpMainPages(final HomeContract.ViewContract view) {
        super(view.getSupportFragmentManager());
        mView = view;
    }

    @Override
    public Fragment getItem(final int position) {
        return (Fragment) mView.getContent().get(position);
    }

    @Override
    public int getCount() {
        return mView.getContent().size();
    }

    @Override
    public CharSequence getPageTitle(final int position) {
        final IFragmentHome home = mView.getContent().get(position);
        Drawable image = ContextCompat.getDrawable(mView.getWeatherfy().getApplicationContext(), home.getIcon());
        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
        SpannableString sb = new SpannableString(" " + mView.getWeatherfy().getString(home.getTitle()));
        ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sb;
    }
}
