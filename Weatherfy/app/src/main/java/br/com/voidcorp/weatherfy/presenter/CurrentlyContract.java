package br.com.voidcorp.weatherfy.presenter;

import android.content.Context;

import java.util.List;

import br.com.voidcorp.weatherfy.helper.net.response.ForecastResponse;
import br.com.voidcorp.weatherfy.model.bo.CurrentlyBO;
import br.com.voidcorp.weatherfy.view.holder.HolderFragCurrently;

public interface CurrentlyContract {

    interface PresenterContract extends ListContract {

        void notifyCurrently(final CurrentlyBO forecast);

        void setAlerts(List<ForecastResponse.Alert> alerts);
    }

    interface ViewContract {

        Context getContext();

        void notifyAlerts();

        HolderFragCurrently getHolder();

    }

}
