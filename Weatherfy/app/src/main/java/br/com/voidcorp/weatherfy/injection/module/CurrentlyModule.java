package br.com.voidcorp.weatherfy.injection.module;

import javax.inject.Singleton;

import br.com.voidcorp.weatherfy.presenter.CurrentlyContract;
import br.com.voidcorp.weatherfy.presenter.impl.CurrentlyPresenterImpl;
import dagger.Module;
import dagger.Provides;

@Singleton
@Module
public class CurrentlyModule {

    private final CurrentlyContract.ViewContract mView;

    public CurrentlyModule(final CurrentlyContract.ViewContract view) {
        mView = view;
    }

    @Provides
    @Singleton
    CurrentlyContract.PresenterContract providesPresenter() {
        return new CurrentlyPresenterImpl(mView);
    }

}
