package br.com.voidcorp.weatherfy.presenter.impl;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.R;
import br.com.voidcorp.weatherfy.helper.task.TaskSearchPlace;
import br.com.voidcorp.weatherfy.model.Searchable;
import br.com.voidcorp.weatherfy.model.bo.CityBO;
import br.com.voidcorp.weatherfy.model.bo.MessageBO;
import br.com.voidcorp.weatherfy.presenter.HomeContract;
import br.com.voidcorp.weatherfy.presenter.SearchContract;
import br.com.voidcorp.weatherfy.view.holder.HolderSearchResult;

public class SearchPresenterImpl implements SearchContract.PresenterContract {

    private final TaskSearchPlace.OnSearchCompletedListener mOnSearchCompletedListener = new TaskSearchPlace.OnSearchCompletedListener() {
        @Override
        public void onSuccess(final List<CityBO> list) {
            mSearchResults.clear();
            if (list.isEmpty()) {
                mSearchResults.add(new MessageBO(mView.getContext().getString(R.string.info_empty_results), true));
            } else {
                mSearchResults.addAll(list);
            }
            mView.notifyDataSetChanged();
        }
    };


    private final SearchContract.ViewContract mView;

    private TaskSearchPlace mTask;
    private List<Searchable> mSearchResults;

    @Inject
    public SearchPresenterImpl(final SearchContract.ViewContract view) {
        mView = view;
        mSearchResults = new ArrayList<>(1);
        mSearchResults.add(new MessageBO(mView.getContext().getString(R.string.info_search), false));
    }

    @Override
    public void search(final String place) {
        cancelSearch();
        mTask = new TaskSearchPlace(mView.getContext(), mOnSearchCompletedListener);
        mTask.execute(place);
    }

    private void cancelSearch() {
        if (mTask != null) {
            mTask.cancel(true);
        }
    }

    @Override
    public void onPlaceSelected(final int position) {
        if(isValidPosition(position)) {
            final Searchable searchable = mSearchResults.get(position);
            if(searchable instanceof CityBO) {
                final CityBO city = (CityBO) searchable;
                if (mView.getWeatherfy().getCityDao().isCityAvailable(city.getName())) {
                    city.setID(mView.getWeatherfy().getCityDao().newID());
                    mView.getWeatherfy().getCityDao().save(city);
                    mView.navigate(HomeContract.ViewContract.CITIES);
                    mView.setSwipeEnabled(true);
                } else {
                    mView.showError(mView.getContext().getString(R.string.error_city_unavailable));
                }
            }
        }
    }

    @Override
    public void clear() {
        cancelSearch();
        mSearchResults.clear();
        mSearchResults.add(new MessageBO(mView.getContext().getString(R.string.info_search), false));
        mView.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mSearchResults.size();
    }

    @Override
    public void putValues(final HolderSearchResult holder, final int position) {
        if (isValidPosition(position)) {
            final int type = getItemViewType(position);
            if (type == Searchable.RESULT) {
                putValues(holder, (CityBO) mSearchResults.get(position));
            } else if (type == Searchable.MESSAGE) {
                putValues(holder, (MessageBO) mSearchResults.get(position));
            }
        }
    }

    @Override
    public int getItemViewType(final int position) {
        if (isValidPosition(position)) {
            return mSearchResults.get(position).getType();
        }
        return -1;
    }

    private boolean isValidPosition(final int position) {
        return position > -1 && position < mSearchResults.size() && !mSearchResults.isEmpty();
    }

    private void putValues(final HolderSearchResult holder, final CityBO city) {
        final AppCompatTextView view = holder.getMessage();
        view.setTextColor(ContextCompat.getColor(mView.getContext(), R.color.tundora));
        view.setText(city.getName());
    }

    private void putValues(final HolderSearchResult holder, final MessageBO message) {
        final AppCompatTextView view = holder.getMessage();
        if(message.isError()) {
            view.setTextColor(ContextCompat.getColor(mView.getContext(), R.color.thunderbird));
        } else {
            view.setTextColor(ContextCompat.getColor(mView.getContext(), R.color.tundora));
        }
        view.setText(message.getMessage());
    }

}
