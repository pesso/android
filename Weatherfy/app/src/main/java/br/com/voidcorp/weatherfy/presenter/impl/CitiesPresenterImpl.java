package br.com.voidcorp.weatherfy.presenter.impl;

import android.view.View;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import br.com.voidcorp.weatherfy.helper.net.ApiManager;
import br.com.voidcorp.weatherfy.helper.net.response.ForecastResponse;
import br.com.voidcorp.weatherfy.model.bo.CityBO;
import br.com.voidcorp.weatherfy.presenter.CitiesContract;
import br.com.voidcorp.weatherfy.presenter.HomeContract;
import br.com.voidcorp.weatherfy.view.holder.HolderForecastCity;
import br.com.voidcorp.weatherfy.view.util.ForecastIconSelector;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CitiesPresenterImpl implements CitiesContract.PresenterContract {

    private final RealmChangeListener<RealmResults<CityBO>> mCityChangeListener = new RealmChangeListener<RealmResults<CityBO>>() {
        @Override
        public void onChange(final RealmResults<CityBO> element) {
            if (mCityCount < getCount()) {
                // added a city
                final CityBO city = getCity(mCityCount);
                mView.notifyItemInserted(mCityCount);
                loadForecast(Collections.singletonList(city));
            } else if (mCityCount > getCount()) {
                mView.notifyItemRemoved(mRemovedItemPosition);
                mRemovedItemPosition = -1;
                if (getCount() == 0) {
                    mView.navigate(HomeContract.ViewContract.SEARCH);
                    mView.setSwipeEnabled(false);
                }
            }

            mCityCount = getCount();


        }
    };


    private final CitiesContract.ViewContract mView;
    private List<CityBO> mCities;
    private int mCityCount;
    private int mRemovedItemPosition = -1;


    public CitiesPresenterImpl(final CitiesContract.ViewContract view) {
        mView = view;
    }

    @Override
    public void load() {
        mCities = mView.getWeatherfy().getCityDao().getCities(mCityChangeListener);
        mCityCount = mCities.size();
        loadForecast(mCities);
        mView.notifyDataSetChanged();
    }

    private void loadForecast(final List<CityBO> cities) {
        for (final CityBO city : cities) {
            city.setLoading(true);
            mView.getWeatherfy().getApi().getForecastFilteredAsync(city.getPosition(), new Callback<ForecastResponse>() {
                @Override
                public void onResponse(final Call<ForecastResponse> call, final Response<ForecastResponse> response) {
                    if (response.isSuccessful()) {
                        mView.getWeatherfy().getCityDao().save(city, response.body().currently, response.body().timezone);
                    } else {
                        city.setError(true);
                    }
                    mView.notifyItemChanged(mCities.indexOf(city));
                }

                @Override
                public void onFailure(final Call<ForecastResponse> call, final Throwable t) {
                    city.setError(true);
                    mView.notifyItemChanged(mCities.indexOf(city));
                }

            }, ApiManager.Filter.DAILY, ApiManager.Filter.HOURLY, ApiManager.Filter.FLAGS, ApiManager.Filter.MINUTELY);
        }
    }

    @Override
    public void onCitySelected(final int position) {
        if (isValidPosition(position)) {
            final CityBO city = getCity(position);
            mView.notifyDetailCity(city.getID(), position);
        }
    }

    @Override
    public int getCount() {
        return mCities == null ? 0 : mCities.size();
    }

    @Override
    public void putValues(final Object obj, final int position) {
        if(obj instanceof  HolderForecastCity) {
            final HolderForecastCity holder = (HolderForecastCity) obj;
            final CityBO city = getCity(position);
            if (city != null) {
                if (city.isLoading()) {
                    holder.getForecast().setVisibility(View.GONE);
                    holder.getLoading().setVisibility(View.VISIBLE);
                } else {
                    holder.getForecast().setVisibility(View.VISIBLE);
                    holder.getLoading().setVisibility(View.GONE);

                    if (city.isError()) {
                        holder.getForecastError().setVisibility(View.VISIBLE);
                        holder.getForecastDegreeType().setVisibility(View.GONE);
                        holder.getForecastIcon().setVisibility(View.GONE);
                        holder.getForecastDegree().setVisibility(View.GONE);
                        holder.getCityName().setVisibility(View.GONE);
                    } else {
                        holder.getForecastError().setVisibility(View.GONE);
                        holder.getForecastDegreeType().setVisibility(View.VISIBLE);
                        holder.getForecastIcon().setVisibility(View.VISIBLE);
                        holder.getForecastDegree().setVisibility(View.VISIBLE);
                        holder.getCityName().setVisibility(View.VISIBLE);

                        if (city.getCurrentlyWeather() != null) {
                            holder.getForecastIcon().setImageResource(ForecastIconSelector.forecast(city.getCurrentlyWeather().icon));
                            holder.getForecastDegreeType().setImageResource(ForecastIconSelector.degree(city.getDegreeType()));
                            if (city.getCurrentlyWeather().temperature != null) {
                                holder.getForecastDegree().setText(String.valueOf((city.getCurrentlyWeather().temperature.intValue())));
                            }
                        }
                        holder.getCityName().setText(city.getName());
                    }
                }
            }
        }
    }

    @Override
    public void notifySelected() {

    }

    @Override
    public void onCityDeleted(final int position) {
        if (isValidPosition(position)) {
            mRemovedItemPosition = position;
            final CityBO city = getCity(position);
            mView.getWeatherfy().getCityDao().delete(city);
        }
    }

    private CityBO getCity(final int position) {
        if (isValidPosition(position)) {
            return mCities.get(position);
        } else {
            return null;
        }
    }

    private boolean isValidPosition(final int position) {
        return position > -1 && position < getCount() && getCount() > 0;
    }

}
