package br.com.voidcorp.weatherfy.view.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import br.com.voidcorp.weatherfy.R;
import br.com.voidcorp.weatherfy.WeatherfyApplication;
import br.com.voidcorp.weatherfy.injection.component.DaggerDetailComponent;
import br.com.voidcorp.weatherfy.injection.module.DetailModule;
import br.com.voidcorp.weatherfy.injection.module.HolderDetailModule;
import br.com.voidcorp.weatherfy.presenter.DetailContract;
import br.com.voidcorp.weatherfy.view.adapter.AdpDetailPages;
import br.com.voidcorp.weatherfy.view.fragment.detail.IFragmentDetail;
import br.com.voidcorp.weatherfy.view.holder.HolderDetailActivity;
import br.com.voidcorp.weatherfy.view.util.SnackbarUtil;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CityDetailActivity extends AppCompatActivity implements DetailContract.ViewContract {

    public static final int REQUEST_CODE = 3925;

    public static final String EXTRA_CITY_ID = "ECI";
    public static final String EXTRA_CITY_POSITION = "ECP";

    private final DialogInterface.OnClickListener mDialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(final DialogInterface dialog, final int which) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                final Intent result = new Intent();
                result.putExtra(EXTRA_CITY_POSITION, mCityPosition);
                setResult(RESULT_OK, result);
                finish();
            }
        }
    };


    @Inject
    DetailContract.PresenterContract mDetailPresenter;

    @Inject
    AdpDetailPages mAdapter;

    @Inject
    List<IFragmentDetail> mDetailFragments;

    @Inject
    HolderDetailActivity mHolder;

    @BindView(R.id.forecast_detail_root)
    View mRoot;

    private AlertDialog mDialog;
    private int mCityPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_detail);
        ButterKnife.bind(this);
        DaggerDetailComponent.builder().detailModule(new DetailModule(this)).holderDetailModule(new HolderDetailModule(mRoot)).build().inject(this);

        setSupportActionBar(getHolder().getToolbar());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mHolder.getViewPager().setAdapter(mAdapter);
        mHolder.getViewPager().setOffscreenPageLimit(5);
        mHolder.getForecastTypeTab().setupWithViewPager(mHolder.getViewPager());

        mCityPosition = getIntent().getIntExtra(EXTRA_CITY_POSITION, -1);

        mDetailPresenter.load(getIntent().getIntExtra(EXTRA_CITY_ID, -1));

    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        return mDetailPresenter.onOptionsItemSelected(item);
    }

    @Override
    public List<IFragmentDetail> getContent() {
        return mDetailFragments;
    }

    @Override
    public HolderDetailActivity getHolder() {
        return mHolder;
    }

    @Override
    public WeatherfyApplication getWeatherfy() {
        return (WeatherfyApplication) getApplication();
    }

    @Override
    public void notifyDelete() {
        if (mDialog == null) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.info_delete_city);
            builder.setPositiveButton(R.string.action_yes, mDialogClickListener);
            builder.setNegativeButton(R.string.action_no, mDialogClickListener);
            mDialog = builder.create();
            mDialog.show();
        } else if (!mDialog.isShowing()) {
            mDialog.show();
        }
    }

    @Override
    public void showError(final String message) {
        SnackbarUtil.show(mHolder.getViewPager(), message);
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void notifyFinish() {
        onBackPressed();
    }

}
