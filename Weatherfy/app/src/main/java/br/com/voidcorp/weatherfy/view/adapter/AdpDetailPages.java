package br.com.voidcorp.weatherfy.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;

import br.com.voidcorp.weatherfy.presenter.DetailContract;

public class AdpDetailPages extends FragmentStatePagerAdapter {

    private final DetailContract.ViewContract mView;

    public AdpDetailPages(final DetailContract.ViewContract view) {
        super(view.getSupportFragmentManager());
        mView = view;
    }

    @Override
    public Fragment getItem(final int position) {
        return (Fragment) mView.getContent().get(position);
    }

    @Override
    public CharSequence getPageTitle(final int position) {
        return mView.getString(mView.getContent().get(position).getTitle());
    }

    @Override
    public int getCount() {
        return mView.getContent().size();
    }
}
